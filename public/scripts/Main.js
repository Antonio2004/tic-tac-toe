var box = document.getElementsByClassName("box");
var WinMessage = document.getElementsByClassName("Message");
var turn = "X";
var tConter = 0; 

console.log(WinMessage[0].innerHTML);

WinMessage[0].innerHTML = "hello";

function PutXO(e){
        if (turn == "X" && !e.classList.contains("O")){
        e.classList.add('X');
        turn = "O";
        tConter++;
        }
        else if(turn == "O" && !e.classList.contains("X")){
        e.classList.add('O');
        turn = "X";
        tConter++;
        }  
}

function Reset(){
    console.log("reset");
    for(let e = 0 ; e < 9 ; ++e ){
        if(box[e].classList.contains("X")){
            box[e].classList.remove('X');
        }

        if(box[e].classList.contains("O")){
            box[e].classList.remove('O');
        }
    }
    tConter = 0;
    turn = "X";
    document.getElementById("Rest").style.display="none";
    WinViewer = setInterval('WinChecker()',100);
}

const WinChecker = () =>{
    if(
    (box[0].classList.contains("X") && box[1].classList.contains("X") && box[2].classList.contains("X")) ||
    (box[0].classList.contains("X") && box[3].classList.contains("X") && box[6].classList.contains("X")) ||
    (box[0].classList.contains("X") && box[4].classList.contains("X") && box[8].classList.contains("X")) ||
    (box[1].classList.contains("X") && box[4].classList.contains("X") && box[7].classList.contains("X")) ||
    (box[2].classList.contains("X") && box[5].classList.contains("X") && box[8].classList.contains("X")) ||
    (box[2].classList.contains("X") && box[4].classList.contains("X") && box[6].classList.contains("X")) ||
    (box[3].classList.contains("X") && box[4].classList.contains("X") && box[5].classList.contains("X")) ||
    (box[6].classList.contains("X") && box[7].classList.contains("X") && box[8].classList.contains("X")))
    {
        WinMessage[0].innerHTML = "X Win";
        console.log(WinMessage[0].innerHTML);
        document.getElementById("Rest").style.display="flex";
        clearInterval(WinViewer);
    }
    else if(
    (box[0].classList.contains("O") && box[1].classList.contains("O") && box[2].classList.contains("O")) ||
    (box[0].classList.contains("O") && box[3].classList.contains("O") && box[6].classList.contains("O")) ||
    (box[0].classList.contains("O") && box[4].classList.contains("O") && box[8].classList.contains("O")) ||
    (box[1].classList.contains("O") && box[4].classList.contains("O") && box[7].classList.contains("O")) ||
    (box[2].classList.contains("O") && box[5].classList.contains("O") && box[8].classList.contains("O")) ||
    (box[2].classList.contains("O") && box[4].classList.contains("O") && box[6].classList.contains("O")) ||
    (box[3].classList.contains("O") && box[4].classList.contains("O") && box[5].classList.contains("O")) ||
    (box[6].classList.contains("O") && box[7].classList.contains("O") && box[8].classList.contains("O")))
    {
        WinMessage[0].innerHTML = "O Win";
        console.log(WinMessage[0].innerHTML);
        document.getElementById("Rest").style.display="flex";
        clearInterval(WinViewer);
    }
    else if(tConter == 9){
        WinMessage[0].innerHTML = "Draw";
        console.log(WinMessage[0].innerHTML);
        document.getElementById("Rest").style.display="flex";
        clearInterval(WinViewer);
    }
}

var WinViewer = setInterval('WinChecker()',100);